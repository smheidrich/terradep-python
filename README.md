# terradep-python

Launcher for Python Terraform providers ensuring dependencies are fulfilled.

DON'T USE THIS YET IF YOU'RE NOT ME! Will hopefully be ready for others to use
at some point. If you're interested in it, let me know and I'll see what I can
do.

Providers themselves can be implemented using e.g.
[tfprovider-python](https://gitlab.com/smheidrich/tfprovider-python).
