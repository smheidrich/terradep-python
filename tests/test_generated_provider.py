import importlib.resources
import os
import re
import shutil
import subprocess
from pathlib import Path
from string import printable

import pytest
from jinja2 import Template
from strip_ansi import strip_ansi

from terradep_python.core import generate_provider_dir

tf_provider_req = """\
terraform {
  required_providers {
    example-provider = {
      source = "local/example-provider"
      version = "0.1.0"
    }
  }
}
"""

tf_provider_install_override_j2 = """\
provider_installation {
  dev_overrides {
    "local/example-provider" = "{{ plugin_dir }}"
  }

  direct {}
}
"""


@pytest.fixture
def required_utils_bin_dir_path(tmp_path):
    """
    Path of directory containing symlinks to programs required by terradep.

    The directory does *not* contain a symlink to a Python 3 binary, which is
    also required by terradep.
    """
    bin_dir = tmp_path / "helpers-bin"
    bin_dir.mkdir()
    # TODO make all these platform-independent
    (bin_dir / "realpath").symlink_to("/usr/bin/realpath")
    (bin_dir / "dirname").symlink_to("/usr/bin/dirname")
    (bin_dir / "mkdir").symlink_to("/usr/bin/mkdir")
    (bin_dir / "cp").symlink_to("/usr/bin/cp")
    return bin_dir


@pytest.fixture
def py3_bin_dir_path(tmp_path):
    bin_dir = tmp_path / "py3-bin"
    bin_dir.mkdir()
    (bin_dir / "python3").symlink_to("/usr/bin/python3")  # TODO platform-ind.
    return bin_dir


@pytest.fixture
def missing_py3_bin_dir_paths(required_utils_bin_dir_path) -> list[Path]:
    return [required_utils_bin_dir_path]


@pytest.fixture
def minimal_working_bin_dir_paths(
    required_utils_bin_dir_path, py3_bin_dir_path
) -> list[Path]:
    """
    Paths to be put into `PATH` to obtain a minimal terradep-suitable env.
    """
    return [required_utils_bin_dir_path, py3_bin_dir_path]


@pytest.fixture
def tf_plugin_dir(tmp_path):
    # plugin dir
    plugin_dir = tmp_path / "plugin"
    plugin_dir.mkdir()
    return plugin_dir


@pytest.fixture
def provider_dir_contents_in_tf_plugin_dir(tf_plugin_dir) -> None:
    generate_provider_dir(tf_plugin_dir, "example-provider", "0.1.0")


@pytest.fixture(scope="module")
def module_scoped_tmp_path(tmp_path_factory):
    return tmp_path_factory.mktemp("testest")


@pytest.fixture(scope="module")
def module_scoped_tf_plugin_dir(module_scoped_tmp_path):
    plugin_dir = module_scoped_tmp_path / "plugin"
    plugin_dir.mkdir()
    return plugin_dir


@pytest.fixture(scope="module")
def module_scoped_tf_plugin_dir_with_provider(module_scoped_tf_plugin_dir):
    """
    This can accelerate tests by only generating a default provider once.

    The generated provider dir can then be re-used across multiple tests by
    just copinyg the files (usually faster than generation).

    The price you pay is of course that you're limited to the defaults, but for
    many tests that is just fine.
    """
    generate_provider_dir(
        module_scoped_tf_plugin_dir, "example-provider", "0.1.0"
    )
    return module_scoped_tf_plugin_dir


@pytest.fixture
def tf_plugin_dir_populated_by_copying_from_module_scoped(
    module_scoped_tf_plugin_dir_with_provider, tf_plugin_dir
):
    shutil.copytree(
        module_scoped_tf_plugin_dir_with_provider,
        tf_plugin_dir,
        dirs_exist_ok=True,
    )
    return tf_plugin_dir


@pytest.fixture
def tf_files(tmp_path):
    # tf file using provider
    (tmp_path / "main.tf").write_text(tf_provider_req)


@pytest.fixture
def tf_rc_file(tmp_path, tf_plugin_dir):
    # tf rc file overriding its source (not registry, local instead)
    rc_file = tmp_path / "terraformrc"
    t = Template(tf_provider_install_override_j2)
    rc_file.write_text(t.render(plugin_dir=tf_plugin_dir))
    return rc_file


@pytest.fixture
def run_tf(
    tf_files,
    tf_plugin_dir_populated_by_copying_from_module_scoped,
    tf_rc_file,
    tmp_path,
    minimal_working_bin_dir_paths,
):
    def run(
        env: dict[str, str] | None = None,
        before_command: str | None = None,
        bin_dir_paths: list[Path] | None = minimal_working_bin_dir_paths,
    ) -> subprocess.CompletedProcess:
        """
        Args:
            bin_dir_paths: Paths to override the `PATH` env var with at the
                beginning of the provider's terradep script (injected via
                `TERRADEP_PRE` env var). Defaults to paths containing symlinks
                to binaries required by terradep, including Python 3. If set to
                `None`, `PATH` will not be overridden at all.
        """
        terradep_pre = ""
        if bin_dir_paths is not None:
            bin_dir_paths_colon_sep = ":".join(str(p) for p in bin_dir_paths)
            terradep_pre += f'export PATH="{bin_dir_paths_colon_sep}"; '
        if before_command is not None:
            terradep_pre += before_command
        env = {
            "TF_CLI_CONFIG_FILE": str(tf_rc_file),
            **({"TERRADEP_PRE": terradep_pre} if terradep_pre else {}),
            **(env or {}),
        }
        cp = subprocess.run(
            "terraform apply",
            shell=True,
            env=env,
            capture_output=True,
            cwd=tmp_path,
        )
        # for easier debugging:
        Path("/tmp/terradep-tests-log.txt").write_text(
            strip_ansi(cp.stderr.decode())
        )
        return cp

    return run


@pytest.fixture
def minimal_package_in_tf_plugin_dir(tf_plugin_dir):
    package_dir = tf_plugin_dir / "example-provider"
    package_dir.mkdir()
    (package_dir / "pyproject.toml").write_text(
        (
            importlib.resources.files("tests")
            / "data/minimal_provider_pyproject.toml"
        ).read_text()
    )
    inner_package_dir = package_dir / "example_provider"
    inner_package_dir.mkdir()
    (inner_package_dir / "__init__.py").touch()
    (inner_package_dir / "__main__.py").write_text(
        """
def run():
  print("well done.")

if __name__ == "__main__":
  run()
"""
    )


def remove_special_and_extra_whitespace(s: str):
    # remove color codes
    s = strip_ansi(s)
    # remove other non-printable
    s = re.sub(f"[^{re.escape(printable)}]+", "", s)
    # replace newlines with spaces
    s = s.replace("\n", " ")
    # remove multiple spaces and newlines
    s = re.sub("[ ]{2,}", " ", s)
    # strip
    s = s.strip(" .")
    return s


def segment(s: str):
    return re.split("[.:] ", s)


def get_expected_output(name: str):
    resource = importlib.resources.files("tests") / f"data/outputs/{name}.txt"
    return resource.read_text()


def prepare_for_comparison(s: str):
    return segment(remove_special_and_extra_whitespace(s))


def get_expected_output_for_comparison(name: str):
    return prepare_for_comparison(get_expected_output(name))


def assert_failed_with_plugin_message(
    cp: subprocess.CompletedProcess, message_name: str
):
    assert cp.returncode != 0
    # 6:-1 to only extract the message itself, not Terraform's surrounding
    # output
    raw_text = cp.stderr.decode()
    text = prepare_for_comparison(raw_text)[6:-1]
    expected_text = get_expected_output_for_comparison(message_name)
    assert text == expected_text


def test_install_instructions(run_tf):
    r = run_tf()
    assert_failed_with_plugin_message(r, "install_instructions")


def test_install_no_python_at_all(run_tf, missing_py3_bin_dir_paths):
    r = run_tf(
        env={"TERRADEP_INSTALL": "1"},
        bin_dir_paths=missing_py3_bin_dir_paths,
    )
    assert_failed_with_plugin_message(r, "no_python_at_all")


def test_install_no_python_at_all_tf_log(run_tf, missing_py3_bin_dir_paths):
    """
    This just checks if TF_LOG=debug changes the error message appropriately.

    Has nothing to do with no Python being being installed, that is just to
    provoke any kind of error.
    """
    cp = run_tf(
        env={"TERRADEP_INSTALL": "1", "TF_LOG": "debug"},
        bin_dir_paths=missing_py3_bin_dir_paths,
    )
    segmented = prepare_for_comparison(cp.stderr.decode())
    assert "See log entries above for detailed error messages" in segmented


def find_pyenv_bin_dir_path_for_version(version: str) -> Path:
    # try to find Python X.Y installation in pyenv dir, skip if not avail
    bin_dir_paths = list(
        Path("~/.pyenv/versions").expanduser().glob(f"{version}*/bin/")
    )
    if len(bin_dir_paths) < 1:
        pytest.skip(f"no Python {version} installation found")
    return bin_dir_paths[0]


@pytest.fixture
def py35_bin_dir_path():
    return find_pyenv_bin_dir_path_for_version("3.5")


@pytest.fixture
def py37_bin_dir_path():
    return find_pyenv_bin_dir_path_for_version("3.7")


def test_install_no_terradep_compatible_python(
    run_tf, py35_bin_dir_path, missing_py3_bin_dir_paths
):
    cp = run_tf(
        env={"TERRADEP_INSTALL": "1", "TF_LOG": "debug"},
        # Python 3.5 is too low for terradep => use that
        bin_dir_paths=missing_py3_bin_dir_paths + [py35_bin_dir_path],
    )
    expected_text_parts = get_expected_output_for_comparison(
        "no_terradep_compatible_python"
    )
    segmented = prepare_for_comparison(cp.stderr.decode())
    for expected_part in expected_text_parts:
        assert expected_part in segmented
    assert (
        "Python interpreter python3 is not able to run terradep helper "
        "scripts because of a version mismatch" in segmented
    )


def test_install_getting_python_version_fails(
    run_tf,
    tmp_path,
    missing_py3_bin_dir_paths,
):
    # simulate broken Python executable => retrieving version fails
    (bin_dir := tmp_path / "bin").mkdir()
    (py_exe_path := bin_dir / "python3").write_text("#!/bin/bash\nfalse")
    py_exe_path.chmod(0o755)
    cp = run_tf(
        env={"TERRADEP_INSTALL": "1", "TF_LOG": "debug"},
        bin_dir_paths=missing_py3_bin_dir_paths + [py_exe_path],
    )
    segmented = prepare_for_comparison(cp.stderr.decode())
    assert (
        "Error trying to find out if Python interpreter python3 is able to "
        "run terradep helper scripts" in segmented
    )
    assert "Moving on" in segmented


def test_install_no_provider_compatible_python(
    run_tf,
    py37_bin_dir_path,
    missing_py3_bin_dir_paths,
):
    cp = run_tf(
        env={"TERRADEP_INSTALL": "1"},
        # Python 3.7 is too low for provider default => use that
        bin_dir_paths=missing_py3_bin_dir_paths + [py37_bin_dir_path],
    )
    assert_failed_with_plugin_message(cp, "no_provider_compatible_python")


def test_installation_happy_path(run_tf, minimal_package_in_tf_plugin_dir):
    """
    Tests installation & running the provider executable.

    Since what's *in* that executable is beyond our responsibility, we just
    check that it runs at all.
    """
    cp = run_tf(
        env={
            "TERRADEP_INSTALL": "1",
            "PATH": os.environ["PATH"],
        }
    )
    assert_failed_with_plugin_message(cp, "installation_happy_path")
